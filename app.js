var express = require('express');
var bodyParser = require('body-parser');
var router = require('./router').router;
var server = express();

server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());
server.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

server.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send("API Crud-Produit");
});

server.use('/api/',router);

server.listen(8080, function(){
    console.log('serveur demaré');
});