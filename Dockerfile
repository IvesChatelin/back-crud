FROM node:20.8.0-alpine3.18

ARG ENV=development
ENV NODE_ENV=${ENV}

WORKDIR /usr/src/app

COPY package*.json ./
COPY package-lock*.json ./

RUN npm install

COPY . .

EXPOSE 8080

CMD [ "node", "app.js" ]