var express = require('express');
var produitCtrl = require('./routes/produitCtrl');

exports.router = (function(){
    var router = express.Router();

    router.route('/produits/add').post(produitCtrl.add);
    router.route('/produits').get(produitCtrl.readAll);
    router.route('/produits/delete/:id').delete(produitCtrl.delete);
    router.route('/produits/update/:id').put(produitCtrl.update);
    router.route('/produits/:nom').get(produitCtrl.readOneProduit);
    
    return router;
})();
